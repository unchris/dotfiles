# cd
alias ..='cd ..'

# ls
alias ls="ls -F -G"
alias l="ls -lAh"
alias ll="ls -l"
alias la='ls -A'

# git
alias g='git'
alias ga='git add'
alias gb='git branch'
alias gc='git commit -m'
alias gca='git commit -a'
alias gcb='git checkout -b'
alias gcd='git checkout develop'
alias gcm='git checkout master'
alias gco='git checkout'
alias gd='git diff'
alias gdc='git diff --cached'
alias gdw='git diff --word-diff'
alias gf='git fetch'
alias gfa='git fetch --all --prune'
alias gk='\gitk --all --branches'
alias gl='git pull --rebase --stat'
alias glog='git log --oneline --decorate --color --graph'
alias gloga='git log --graph --decorate --oneline --color --all'
alias glogd='git log --graph --decorate $(git rev-list -g --all)'
alias gm='git merge'
alias gp='git push'
alias gr='git remote'
alias grb='git rebase'
alias grba='git rebase --abort'
alias grbc='git rebase --continue'
alias grbi='git rebase -i'
alias grbs='git rebase --skip'
alias grm="git status | grep deleted | awk '{print \$3}' | xargs git rm"
alias gs='git status'
alias gss='git status --short'

# rails
alias rt="rails test"
alias rc='rails console'
alias rs='rails server -b 0.0.0.0'
alias rss='puma -b "ssl://127.0.0.1:3000?key=$HOME/.ssh/server.key&cert=$HOME/.ssh/server.crt"'
alias rg='rails generate'
alias migrate='rails db:migrate'
alias be='bundle exec'
alias rfind='find . -name "*.rb" | xargs grep -n'  # Find ruby file

# python
alias activate='. env/bin/activate'
alias py='python2.7'
alias pyc="./scripts/console.sh"
alias pycl='find . -name "*.pyc" -delete && find . -name "__pycache__" -delete'
alias pys="py runserver.py"

#redshift
alias rswarm='redshift -O 4000'
alias rscool='redshift -O 6000'

# other
alias rsync="rsync --progress"
alias tmux="TERM=screen-256color-bce tmux -2"
alias ssh-keygen='ssh-keygen -b 4096'
