#compdef dsetool

__dsetool_commands () {
  local -a commands; commands=(
    'core_indexing_status:<keyspace.cf> - Returns the current indexing status (INDEXING, FAILED, FINISHED) of a given core'
    'create_core:<keyspace.cf> schema=<path> solrconfig=<path> distributed=<true|false> recovery=<true|false> deleteAll=<true|false> reindex=<true|false> generateResources=<true|false>'
    'get_core_config:<keyspace.cf> <current=true|false> - Outputs the latest uploaded solrconfig. If current is set to true returns the current live one.'
    'get_core_schema:<keyspace.cf> <current=true|false> - Outputs the latest uploaded schema. If current is set to true returns the current live one.'
    'infer_solr_schema:<keyspace.cf> - Returns a proposal schema for the specified Keyspace.ColumnFamily'
    'inmemorystatus:Returns a list of the in-memory tables for this node and the amount of memory each table is using, or information about a single table if the keyspace and columnfamily are given.'
    'node_health:Returns a health score between 0 and 1 for the node'
    'read_resource:<keyspace.cf> <name=name of the resource we want to read> - Reads a DSE Search resource file.'
    'reload_core:<keyspace.cf> schema=<path> solrconfig=<path> distributed=<true|false> deleteAll=<true|false> reindex=<true|false> - Reloads a core with the given keyspace and cf/table name'
    'ring:List the nodes in the ring including their node type.'
    'status:List the nodes in the ring including their node type.'
    'unload_core:<keyspace.cf> deleteDataDir=<true|false> deleteResources=<true|false> distributed=<true|false> - Effectively destroys a core without modifying its backing table.'
  )
  _describe -t commands 'dsetool command' commands
}

__dsetool_cache_keyspace () {
  if [ ! -f /tmp/cassandra-keyspaces-zsh ]; then
    cqlsh -e 'select keyspace_name,columnfamily_name from system.schema_columnfamilies;' | grep '|' | grep -v keyspace_name | sed -e s/\|/\./ | sed -e 's/\ //g' > /tmp/cassandra-keyspaces-zsh
  fi
}

__dsetool_keyspace_arg () {
  __dsetool_cache_keyspace
  compadd $(cat /tmp/cassandra-keyspaces-zsh)
}

__dsetool_sub_commands () {
  case "$words[1]" in
    (help)
      _arguments -C \
        '(-): :->help-command'
      case "$state" in
        (help-command) __dsetool_commands ;;
      esac
    ;;
    (core_indexing_status) __dsetool_keyspace_arg ;;
    (create_core) __dsetool_keyspace_arg ;;
    (get_core_config) __dsetool_keyspace_arg ;;
    (get_core_schema) __dsetool_keyspace_arg ;;
    (infer_solr_schema) __dsetool_keyspace_arg ;;
    (read_resource) __dsetool_keyspace_arg ;;
    (reload_core) __dsetool_keyspace_arg ;;
    (unload_core) __dsetool_keyspace_arg ;;
  esac
}

_dsetool () {
  local -a common_args; common_args=(
    '(-a|--jmxusername)'{-a,--jmxusername}'[JMX user name]' \
    '(-b|--jmxpassword)'{-b,--jmxpassword}'[JMX password]' \
    '(-c|--cassandra_port)'{-c,--cassandra_port}'[Cassandra port to use]' \
    '(-f|--config-file)'{-f,--config-file}'[DSE configuration file]' \
    '(-h|--host)'{-h,--host}'[node hostname or ip address]' \
    '(-j|--jmxport)'{-j,--jmxport}'[remote jmx agent port number]' \
    '(-l|--username)'{-l,--username}'[User name]' \
    '(-p|--password)'{-p,--password}'[Password]' \
    '(-s|--port)'{-s,--port}'[Solr port to use]' \
    '(-u|--use_hadoop_config)'{-u,--use_hadoop_config}'[get cassandra host from hadoop configuration]'
  )

  _arguments -C \
    '(-): :->command' \
    '(-)*:: :->arg' \
    $common_args

  case "$state" in
    (command)
      __dsetool_commands
    ;;
    (arg)
      __dsetool_sub_commands
    ;;
  esac
}

_dsetool "$@"
